Numworks

It can be computed using the probability density function of the binomial distribution: `binompdf` available since version 12.2 (2019-11).

```
binompdf(1515, 3030, 0.5)
0.01449382 (1.4498321698E-2)
```

Note that `binomial(3030,1515)` or `3030!` overflows.
(`binomial(N,x) = \binom{N}{x}`)
